            .data 0x10010000
var1:       .word 0x10              # var1 is a word (32 bit) with the initial value 0x10

var2:       .word 0x20              # var1 is a word (32 bit) with the initial value 0x20

var3:       .word 0x30              # var1 is a word (32 bit) with the initial value 0x30

var4:       .word 0x40              # var1 is a word (32 bit) with the initial value 0x40 

first:      .byte 'X'               # first is a byte(8 bit) with the initial value 'X'

last:       .byte 'Z'               # last is a byte(8 bit) with the initial value 'Z' 

            .text
            .globl main 

main:       addu $s0, $ra, $0       # save $31 in $16
            lui $t8, 4097           # load var1 to register t0
            lw $t0, 0($t8)
            lui $t8, 4097           # load var2 to register t1
            lw $t1, 4($t8)
            lui $t8, 4097           # load var3 to register t2
            lw $t2, 8($t8)
            lui $t8, 4097           # load var4 to register t3
            lw $t3, 12($t8)  
            lui $t8, 4097           # load first to register t4
            lw $t4, 16($t8)
            lui $t8, 4097           # load last to register t5
            lw $t5, 17($t8)
            lui $t8, 4097           # store the value in registert3 to var1
            sw $t3, 0($t8)
            lui $t8, 4097           # store the value in registert2 to var2
            sw $t2, 4($t8)
            lui $t8, 4097           # store the value in registert1 to var3
            sw $t1, 8($t8)
            lui $t8, 4097           # store the value in registert0 to var4
            sw $t0, 12($t8)
                                    # restore now the return address in $ra and return from main
            addu $ra, $0, $s0       # return address back in $31
            jr $ra                  # return from main