            .data 0x10010000
var1:       .word 0x14              # var1 is a word (32 bit) with the initial value 0x14

var2:       .word 0x3E              # var1 is a word (32 bit) with the initial value 0x3E

            .extern ext1 4

            .extern ext2 4


            .text
            .globl main 

main:       addu $s0, $ra, $0       # save $31 in $16
            lw $t0, var1            # load var1 to register t0
            lw $t1, var2            # load var2 to register t1
            sw $t0, ext1            # store the value in registert0 to ext1 
            sw $t1, ext2            # store the value in registert1 to ext2 