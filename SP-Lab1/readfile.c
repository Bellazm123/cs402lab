/*
 * File read/write module implementation. 
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 */

#include "readfile.h"


FILE *open_file(char *filename, char *sc)
{	
	FILE *fp;
 	if ((fp =fopen(filename, sc)) == NULL){
 		printf("Fail to open Data file!\n");
 		exit(0);
	 }
	 return fp;
};

int read_int(int *path)
{
	if (scanf("%d", path) == 0){
		setbuf(stdin, NULL);
		return -1;
	} else {
		return 0;
	}
};

int read_string(char *path)
{
	if (scanf("%s", path) == 0){
		setbuf(stdin, NULL);
		return -1;
	} else {
		return 0;
	}	
};

int read_float(float *path)
{
	if (scanf("%f", path) == 0){
		setbuf(stdin, NULL);
		return -1;
	} else {
		return 0;
	}	
};

void close_file(FILE *filename)
{
	fclose(filename);	
};

