            .data 0x10000000
msg1:       .asciiz "Please enter an integer number: "
msg2:       .asciiz "I'm far away."
msg3:       .asciiz "I'm nearby"
adr:        .word 1

            .text
            .globl main
        

main:       addu $s0, $ra, $0       # save $31 in $16
            li $v0, 4               # system call for print_str
            la $a0, msg1            # address of string to print
            syscall        
            li $v0, 5               # system call for read_int
            syscall                 # the integer placed in $v0
            addu $t0, $v0, $0       # move the number in $t0     
            li $v0, 4               # system call for print_str
            la $a0, msg1            # address of string to print
            syscall
            li $v0, 5               # system call for read_int
            syscall                 # the integer placed in $v0 
            addu $t1, $v0, $0       # move the number in $t1
            
            beq $t0, $t1, Far       # if $t0 == $t1,Far
            
            li $v0, 4               # system call for print_str
            la $a0, msg3            # address of string to print
            syscall           
            j Exit
        

Far:        li $v0, 4               # system call for print_str
            la $a0, msg2            # address of string to print
            syscall
            b adr                   # go to farther than a 16 bit offset

Exit:
            # restore now the return address in $ra and return from main    
            addu $ra, $0, $s0       # return address back in $31
            jr $ra                  # return from main