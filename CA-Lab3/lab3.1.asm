            .data 0x10010000
var1:       .word 0x2               # var1 is a word (32 bit) with the initial value 2

var2:       .word 0x0               # var2 is a word (32 bit) with the initial value 0

var3:       .word 0xFFFFF81C        # var3 is a word (32 bit) with the initial value -2020

            .text
            .globl main
main:       addu $s0, $ra, $0       # save $31 in $16
            lw $t0, var1            # load var1 to register t0
            lw $t1, var2            # load var2 to register t1
            
            bne $t0, $t1, Else      # go to Else if $t0 != $t1
            lw $t2, var3            # load var3 to register t2
            sw $t2, var2            # store the value in registert2 to var2
            sw $t2, var1            # store the value in registert2 to var1
            beq $0, $0, Exit        # go to Exit
            
Else: 
            move $t2, $t0           # swap $t0 and $t1
            move $t0, $t1
            move $t1, $t2
            sw $t0, var1            # store the value in registert1 to var1
            sw $t1, var2            # store the value in registert0 to var2

Exit:
            li $v0, 1               # print_int
            lw $a0, var1
            syscall
            li $v0, 1
            lw $a0, var2
            syscall           
                                    # restore now the return address in $ra and return from main
            addu $ra, $0, $s0       # return address back in $31
            jr $ra                  # return from main