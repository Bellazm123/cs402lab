                .data 0x10000000
user1:          .word 0
msg1:           .asciiz "Please enter an integer number: "
msg2:           .asciiz "If bytes were layed in reverse order the number would be: "
                
                .text
                .globl main
main:           addi $sp, $sp, -4
                sw $ra, 4($sp)      # save $ra in $sp
                li $v0, 4
                la $a0, msg1
                syscall

                li $v0, 5
                syscall
                
                sw $v0, user1
                la $a0, user1
                jal Reverse_bytes
                li $v0, 4
                la $a0, msg2
                syscall
                
                lw $t5, user1
                li $v0, 1
                move $a0, $t5
                syscall
                
                lw $ra, 4($sp)      # restore the return address in $ra
                addi $sp, $sp, 4
                jr $ra              # return from mian
                               
Reverse_bytes:   
                lb $t1, 0($a0)
                lb $t2, 1($a0)
                lb $t3, 2($a0)
                lb $t4, 3($a0)
                sb $t4, 0($a0)
                sb $t3, 1($a0)
                sb $t2, 2($a0)
                sb $t1, 3($a0)
                jr $ra              # return from this procedure