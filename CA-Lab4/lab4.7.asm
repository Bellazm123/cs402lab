                .data 0x10000000
msg1:           .asciiz "Please enter first positive integer: "   
msg2:           .asciiz "Please enter second positive integer: "             
msg3:           .asciiz "error, please re-enter a positive integer: " 
msg4:           .asciiz "The value of the Ackermann's function is: "              
                
                .text
                .globl main
                
main:           add $s0, $ra, $0        # save $31 in $16
                li $v0, 4               # system call for print_str
                la $a0, msg1            # address of string to print
                syscall
                li $v0, 5               # system call for read_int
                syscall                 # the integer placed in $v0
                addu $t0, $v0, $0       # move the number in $t0
                
test1:          slt $t2, $0, $t0        # $t2 = 0 if $t0 < 0
                bltz $t2, renter1
                j second
                
renter1:        li $v0, 4               # system call for print_str
                la $a0, msg3            # address of string to print
                syscall
                li $v0, 5               # system call for read_int
                syscall                 # the integer placed in $v0
                addu $t0, $v0, $0       # move the number in $t0 
                j test1
                
second:         li $v0, 4               # system call for print_str
                la $a0, msg2            # address of string to print
                syscall
                li $v0, 5               # system call for read_int
                syscall                 # the integer placed in $v0
                addu $t1, $v0, $0       # move the number in $t0
                
test2:          slt $t2, $0, $t1        # $t2 = 0 if $t1 < 0
                bltz $t2, renter2
                j goA                

renter2:        li $v0, 4               # system call for print_str
                la $a0, msg3            # address of string to print
                syscall
                li $v0, 5               # system call for read_int
                syscall                 # the integer placed in $v0
                addu $t1, $v0, $0       # move the number in $t0 
                j test2  

goA:            move $a0, $t0
                move $a1, $t1
                jal Ackermann           # call 'Ackermann' with two parameters
                addu $s1, $v0, $0       # Save the results to $s1
                
                li $v0, 4               # system call for print_str
                la $a0, msg4            # address of string to print
                syscall
                li $v0, 1               # system call for print_int
                move $a0, $s1           
                syscall
                                       
exit:           add $ra, $s0, $0        # save $16 in $31
                jr $ra
                
Ackermann:      
                bne $a0, $0, branch2    # branch1, Don't need to stack
                addi $v0, $a1, 1
                jr $ra
                
branch2:
                bne $a1, $0, branch3    # branch2, save $ra in stack
                subu $a0, $a0, 1
                addu $a1, $0, 1
                subu $sp, $sp, 4
                sw $ra, 4($sp)
                jal Ackermann 
                lw $ra, 4($sp)
                addu $sp, $sp, 4
                jr $ra
                
branch3:                
                subu $a1, $a1, 1        # branch3, save $ra,$a0 in stack
                subu $sp, $sp, 8
                sw $a0, 4($sp)
                sw $ra, 8($sp)
                jal Ackermann 
                lw $a0, 4($sp)
                subu $a0, $a0, 1
                move $a1, $v0
                jal Ackermann
                lw $ra, 8($sp)
                addu $sp, $sp, 8
                jr $ra
                
                
                
                
                
                
                
                
                
                
                
                