            .data 0x10000000
msg1:       .asciiz "Please enter an integer number: "
msg2:       .asciiz "The maximum value of two is:"
                
            .text
            .globl main
main:       addi $sp, $sp, -4
            sw $ra, 4($sp)          # must save $ra since I'll have a call
                
            li $v0, 4               # system call for print_str
            la $a0, msg1            # address of string to print
            syscall
            li $v0, 5               # system call for read_int
            syscall                 # the integer placed in $v0 
            addu $t0, $v0, $0       # move the number in $t0
            
            li $v0, 4               # system call for print_str
            la $a0, msg1            # address of string to print
            syscall
            li $v0, 5               # system call for read_int
            syscall                 # the integer placed in $v0 
            addu $t1, $v0, $0       # move the number in $t0
            
            move $a0, $t0
            move $a1, $t1            
            jal Largest             # call 'Largest' with two parameters
            
            li $v0, 4               # system call for print_str
            la $a0, msg2            # address of string to print
            syscall
            li $v0, 1               # system call for print_int
            add $a0, $v1, $0        # integer to print
            syscall
            
            
            lw $ra, 4($sp)          # restore the return address in $ra
            addi $sp, $sp, 4
            jr $ra                  # return from mian
            

            
Largest:    move $t3, $a0
            move $t4, $a1
            slt $t5, $t3, $t4       # $t5 = 1 if $t3 < $t4
            beq $t5, $0, max1
            move $v1, $t4           # the maximum value is $t4
            jr $ra
            
max1:       move $v1, $t3           # the maximum value is $t3
            jr $ra 