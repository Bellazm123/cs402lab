                .data 0x10000000
                
                .text
                .globl main
main:           move $s0, $ra       # must save $ra since I'll have a call
                jal test            # call 'test' with no parameters
                nop                 # execute this after 'test' returns
                move $ra, $s0       # restore the return address in $ra
                jr $ra              # return from mian
                
                # The procedure 'test' dose not call any ather procedure. Therefore $ra
                # dose not need to be saved . Since 'tesst' uses no registers there is
                # no need to save any registers.
                
test:           nop                 # this is the procedure named 'test'
                jr $ra              # return from this procedure