﻿# C - based employee database

# Project structure

 - Main  : program entry
	 - main.c
 - DB_control  : Database control module
	 - DB_control.h
	 - DB_control.c
 - Readfile : File read-Write module
	 - readfile.h
	 - readfile.c
 - Display : Page display module
	 - displaypage.h
	 - displaypage.c

# Partial  function realization
 - Lookup employee by ID
	 - By binary search.
 - Print the M employees with the highest salaries
	 - Build a min-heap of size M, Iterate through the array to get the salary of the previous M size.
	 - Get the descending result by min-heap sort.

# Development environment
Developers are advised to use the following environment to avoid problems with the release
 - Linux
 - gcc version 9.3.0

# Compiling
 - gcc -c main.c -o main.o
 - gcc -c displaypage.c -o displaypage.o
 - gcc -c readfile.c -o readfile.o
 - gcc -c DBcontrol.c -o DBcontrol.o
# Link
 - gcc main.o DBcontrol.o displaypage.o readfile.o -o workerDB 

