/*
 * DB_control module header file
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 * 2020-11-10	  Xiyang.Zhang     Add new function
 */

#include "displaypage.h"

// Defining an array of employees.
struct Employee EeDB[MAXARR+1];  
// Define the maximum employee ID.
int max_id;
// Define the number of employees.
int num;

// Initializes the database with a data file.
void init_DB(char *file);

// Sort the employee array by ID.
int sort_DB_by_ID();

// Add an employee.
int add_ee(void);

// Binary search for employees by ID, retuen index of array.
int binary_search(int target); 

// Binary search for employees by ID.
void search_by_id();

// Search for employees by last name.
void search_by_last_name();

// Display database data.
void display();

// Save the data back to the data file.
void save_DB(char *file);

// Remove an employee by Id.
void del_by_id();

// Update an employee's information by Id.
void update_by_id();

// Print the M employees with the highest salaries
void topm();

// Find all employees with matching last name
void search_all_by_last_name();



//-------------- min-heap -------
// The size of the heap.
static int size;

// Element exchange
void swap(struct Employee *heap, int i, int j);

// Compute the parent node index
int parent(int i);

// Compute the left child node index
int left(int i);

// Compute the right child node index
int right(int i);

// min-heap repair
void min_heapify(struct Employee *heap, int i);

// Build the min-heap from the array
void build_min_heap(struct Employee *heap, int num);

//  Descending sort
void heap_sort(struct Employee *heap);



