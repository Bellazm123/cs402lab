/*
 * C - based employee database. You can present data, add employees, 
 * and find employees by ID or last name.
 *
 * This is the main function of the program 
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 * 2020-11-10	  Xiyang.Zhang     Add new function
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <malloc.h> 
#include "DBcontrol.h"

int main(int argc, char *argv[]) {
	// Database initialization
	init_DB(argv[1]);
	
	int flag; 		// Success failure mark 
	int option;		// Option variable
	while(1){
		// Options page
		menu();
		// Read user options
		flag = read_int(&option);
		if(flag != 0){
			printf("Incorrect input format, Please enter an integer between 1 and 9.\n");
			printf("\n");
			continue;
		}
		printf("\n");
		switch (option)
		{	
			case 1: display(); break;
			case 2: search_by_id(); break;
			case 3: search_by_last_name(); break;
			case 4: add_ee(); break;
			case 5: save_DB(argv[1]);return 0; break;
			case 6: del_by_id(); break;
			case 7: update_by_id();break;
			case 8: topm();break;
			case 9:	search_all_by_last_name();break;
			default: printf("Hey, %d is not between 1 to 9, please make your choice again.\n",option); break;
		}
	}
	return 0;
};
