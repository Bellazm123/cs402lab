/*
 * index calculation module header file
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-17     Xiyang.Zhang     first version
 */

#include <math.h>


// function to get mean of values;
double get_mean(float *arr, int num);

// function to get sd
double get_sd(float *arr, int num);

// function to get median
double get_median(float *arr, int num);

// function to sort array
void sort(float *arr, int num);

// selective sorting algorithm
float select_sort(float *arr, int num);

// quick sort algorithm
void quick_sort(float *arr, int first, int end);

//  quicksort partition function 
int partition(float *arr, int first, int end);

// function to swap items;
void swap(float *xp, float *yp);
